#COMP6001 - GUI Programming
======
#Assessment 1
###Cory Bruin - 9961251

**1. Keep the interface simple.**
Often referred to as; KISS - Keep it simple, stupid! The best interfaces are basic and intuitive. The easier something is to use, the more likely it is to be adopted and engaged with. Avoid adding any UI elements that complicate the page without adding any value to the user. It's important to design the layout and functionality based on the knowledge, skills and abilities of the people who are going to be using your website.


**2. Create consistency and use common UI elements.**
Ensure the layout has a theme and make sure that same theme follows through onto every page of your website. Creating a pattern and style used through out means a users become comfortable with the layout and is able to get things done more quickly. Once a user learns how to do something then they can carry that knowledge to other areas of the site.


**3. Be purposeful in page layout.**
Design the website so important user interface elements are in the most obvious place on the page. Each page should have a specific purpose, one reason for why it exists and the users instant focus should be directed to that purpose. Layout basically consists of four visual parts: Header, footer, right-side and/or left-side panels, and a content section. Each of these parts should state their purpose. Place the most important things on the top part the page that can be seen without scrolling.

**4. Strategically use color and texture.** 
Directing attention can be achieved by using colour, light, constrast, and texture to highlight a point of interest on the page. When using traditional colour conventions, for example, use a green button when submitting forms, red button for cancelling. When designing, color is used as a subliminal tool to evoke certain emotions and connotations.

**5. Use typography to create hierarchy and clarity.** 
Carefully consider how you use text on the website. Different sizes, fonts, and arrangement of the text to help increase scanability, legibility and readability. a study at ACM CHI 2016, the premier scientific conference on human factors provides evidence that objective readability (measured via eye-tracking) and comprehension of texts (number of correctly answered comprehension question) significantly improves when using larger-than-usual font sizes (18, 22, 26 points). Chrome and Firefox default at 12 points, the study shows that size 12 impairs readability to the point that comprehensipon is affected. Keep the text to a minimum; in fact, according to a study by the Norman Nielsen Group, your visitors will only ready between 20 and 28% of the words on your site.

**6. Make sure that the system communicates what’s happening.**  
Communication is sometimes overlooked and takes a backseat to the visual attractiveness of a website. Ideally, the design and other elements that do the communicating work together to create a clear, unified message to visitors. Display messages informing the user detailing the results of the action they've performed, this helps the learning curve of the user. Informing the user their action has had a result reduces confusion and improves effeciency when using the website.

**7. Think about the defaults.** 
By carefully thinking about and anticipating the goals people bring to your site, you can create defaults that reduce the burden on the user.  This becomes particularly important when it comes to form design where you might have an opportunity to have some fields pre-chosen or filled out. In forms and applications, pre-populate fields with the most common value if you can determine it in advance. By educating and guiding users, default values help reduce errors. It's therefore important to select helpful defaults, rather than those based on the first letter of the alphabet or whatever the first option on your original list happened to be. Users rely on defaults in many other areas of user interface design. For example, they rarely utilize fancy customization features, making it important to optimize the default user experience, since that's what most users stick to.

**8. Vocabulary and Terminology**
The majority of the users visiting the website will have no programming knowledge - therefore leave any programming terminology or vocabulary off of your website. Any interface that is limited to the use of a small vocabulary. This applies to command-line systems, but usually refers to natural language and spoken language systems that, in order to produce consistent, predictable results, limit the number of recognized words. A controlled vocabulary is also a design approach that provides a styleguide with a list of prescribed terminology to ensure consistency in vocabulary and usage in your content domain.


**Peer Review**  
By Johnathon Proffitt


This Report covers many of the same topics as mine. It includes the key points of simplicity and accesibility. It also covers the importance of communicating clearly with the user, to ensure they know what is happeneing at all times. 10/10.

References:  
https://www.interaction-design.org/literature/article/kiss-keep-it-simple-stupid-a-design-principle  
https://www.usability.gov/what-and-why/user-interface-design.html  
https://stackoverflow.com/questions/90813/best-practices-principles-for-gui-design  
http://ptgmedia.pearsoncmg.com/images/9780321683687/samplepages/0321683684.pdf  
http://pielot.org/2016/01/optimal-font-size-for-web-pages/  
https://www.smashingmagazine.com/2009/02/clear-and-effective-communication-in-web-design/  
https://www.nngroup.com/articles/the-power-of-defaults/  
https://www.devsaran.com/blog/how-color-influences-website-usability  
http://mobilestorm.com/website-usability/web-usability-layout/  

